package pe.edu.uni.fiis.fcp.matrigen.service;

import pe.edu.uni.fiis.fcp.matrigen.service.horario.HorarioService;
import pe.edu.uni.fiis.fcp.matrigen.service.horario.HorarioServiceImpl;
import pe.edu.uni.fiis.fcp.matrigen.service.update.UpdateNoteService;
import pe.edu.uni.fiis.fcp.matrigen.service.update.UpdateNoteServiceImpl;

public abstract class SingletonService {
    private static HorarioService horarioService=null;
    public static HorarioService getHorarioService(){
        if(horarioService==null){
            horarioService=new HorarioServiceImpl();
        }
        return horarioService;
    }

    private static UpdateNoteService updateNoteService=null;
    public static UpdateNoteService getUpdateNoteService(){
        if(updateNoteService==null){
            updateNoteService=new UpdateNoteServiceImpl();
        }
        return updateNoteService;
    }
}
