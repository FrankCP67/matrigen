package pe.edu.uni.fiis.fcp.matrigen.service.update;

import pe.edu.uni.fiis.fcp.matrigen.model.UpdNote;

public interface UpdateNoteService {
    public UpdNote obtenerLastUpdate();
}
