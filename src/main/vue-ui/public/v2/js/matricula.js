const User = {
    template: '<div>User {{ $route.query.id }}</div>'
}

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/generar',
            component: User,
            props: route => ({query: route.query.id})
        }
    ]
})

let app = new Vue({
    router,
    el: '#app',
    data: {
        pagina: 3,
        subpagina: 1,
        searchCurso: "",
        listaCursos: null,
        cursosCreditos: 0,
        listaHorarios: null,
        horarioGen: null,
        horarioGenControl: [],
        horarioCrucesControls: [],
        horarioCrucesInfo: [],
        cursoSec: {
            curso: [],
            sec: [],
        },
        cursoCruces: [],
        colorCursos: ['#ff6e6e', '#aebbff', '#ffee58', '#d0fda0',
            '#f4a6d4', '#bf7df3', '#f6c470', '#9ceeb9',
            '#81a57d', '#8bceff', '#867d6e'],
        savedUpd: {
            id: null,
            fecha: null,
            hora: null,
            descripcion: 'Espere...'
        },
        listadoCursosSel: [],
        popupText: null,
        listadoComb: [],
        isGenerado: false,
        parametro: null
    },
    mounted() {
        this.start();
    },
    computed: {
        searchCursoList() {
            return this.listaCursos.filter(curso =>
                curso.nombre.toLowerCase().includes(this.searchCurso.toLowerCase()) ||
                curso.codigo.toLowerCase().includes(this.searchCurso.toLowerCase())
            )
        },
        selectedCursoList() {
            return this.listaCursos.filter(curso =>
                this.listadoCursosSel.indexOf(curso.codigo) !== -1
            )
        }
    },
    methods: {
        start: function () {
            bulmaToast.toast({
                message: 'Obteniendo cursos...',
                type: 'is-link',
                duration: 2000,
                position: 'bottom-center',
                dismissible: false,
                animate: {in: 'fadeIn', out: 'fadeOut'},
            });
            this.parametro = this.$route.query.id;
            this.getLastUpdate();
            this.mostrarCursos();
            Vue.$cookies.config("30d", "", "", true);
        },
        getLastUpdate: function () {
            let that = this;
            fetch('../api/gluc', {
                method: 'POST',
                body: '{}',
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (res) {
                return res.json();
            })
                .then(function (data) {
                    that.cursosCreditos = JSON.parse(that.$cookies.get("num"));
                    data.fecha = new Intl.DateTimeFormat('es-PE').format(new Date(data.fecha))
                    that.savedUpd = JSON.parse(JSON.stringify(data));
                });
        },
        mostrarCursos: function () {
            let that = this;
            fetch('../api/obtener-cur', {
                method: 'POST',
                body: '{}',
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (res) {
                return res.json();
            })
                .then(function (data) {
                    that.listaCursos = data;
                    that.limpiarSel();
                    if (that.$cookies.get("savedCursos") !== null) {
                        that.listadoCursosSel = JSON.parse(that.$cookies.get("savedCursos"));
                    }
                    if (that.$cookies.get("Comb") !== null) {
                        that.listadoComb = JSON.parse(that.$cookies.get("Comb"));
                    }
                });
        },
        limpiarSel: function () {
            this.listadoCursosSel = [];
        },
        limpiarSearch: function () {
            this.searchCurso = "";
        },
        removeA: function (arr) {
            let what, a = arguments, L = a.length, ax;
            while (L > 1 && arr.length) {
                what = a[--L];
                while ((ax = arr.indexOf(what)) !== -1) {
                    arr.splice(ax, 1);
                }
            }
            return arr;
        },
        addCursoSel: function (index) {
            let item = this.searchCursoList[index];
            if (this.selectedCursoList.indexOf(item) === -1) {
                this.listadoCursosSel.push(item.codigo);
                bulmaToast.toast({
                    message: 'Se agregó: ' + item.codigo + ' - ' + item.nombre,
                    type: 'is-black',
                    duration: 2000,
                    position: 'bottom-right',
                    dismissible: false,
                    animate: {in: 'fadeIn', out: 'fadeOut'},
                })
                this.$cookies.set("savedCursos", JSON.stringify(this.listadoCursosSel));
            } else {
                bulmaToast.toast({
                    message: 'Ya se encuentra añadido: ' + item.codigo + ' - ' + item.nombre,
                    type: 'is-danger',
                    duration: 2000,
                    position: 'bottom-right',
                    dismissible: false,
                    animate: {in: 'fadeIn', out: 'fadeOut'},
                })
            }
        },
        removeCursoSel: function (i) {
            let item = this.selectedCursoList[i];
            bulmaToast.toast({
                message: 'Eliminado: ' + item.codigo + ' - ' + item.nombre,
                type: 'is-danger',
                duration: 2000,
                position: 'bottom-right',
                dismissible: false,
                animate: {in: 'fadeIn', out: 'fadeOut'},
            })
            this.removeA(this.listadoCursosSel, this.selectedCursoList[i].codigo);
            this.$cookies.set("savedCursos", JSON.stringify(this.listadoCursosSel));
        },
        convertirCursoSec: function () {
            this.cursoSec.curso = [];
            this.cursoSec.sec = [];
            let inputsCursoSec = document.getElementsByClassName("cursoSelec")
            let indICS = 0;
            for (let i = 0; i < this.selectedCursoList.length; i++)
                for (let j = 0; j < this.selectedCursoList[i].secciones.length; j++) {
                    if (inputsCursoSec[indICS].checked) {
                        this.cursoSec.curso.push(this.selectedCursoList[i].codigo);
                        this.cursoSec.sec.push(this.selectedCursoList[i].secciones[j]);
                    }
                    indICS++;
                }
        },
        generarHorario: function () {
            this.convertirCursoSec();
            this.generarHorarioFrom(this.cursoSec);
        },
        generarHorarioFrom: function (cursosec) {
            let that = this;
            this.isGenerado=true;
            this.horarioGenControl = [];
            this.listaHorarios = [];
            this.horarioCrucesControls = [];
            this.horarioCrucesInfo = [];
            this.horarioGen = {
                8: [null, null, null, null, null, null],
                9: [null, null, null, null, null, null],
                10: [null, null, null, null, null, null],
                11: [null, null, null, null, null, null],
                12: [null, null, null, null, null, null],
                13: [null, null, null, null, null, null],
                14: [null, null, null, null, null, null],
                15: [null, null, null, null, null, null],
                16: [null, null, null, null, null, null],
                17: [null, null, null, null, null, null],
                18: [null, null, null, null, null, null],
                19: [null, null, null, null, null, null],
                20: [null, null, null, null, null, null],
                21: [null, null, null, null, null, null],
            };
            this.setSubPag(1);
            this.setPagina(2);
            this.popup_generado(1, "Cargando Horario...");
            fetch('../api/obtener-hora', {
                method: 'POST',
                body: JSON.stringify(cursosec),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (res) {
                return res.json();
            })
                .then(function (data) {
                    that.listaHorarios = data;
                    that.initControlHorarioGen(0);
                    that.generarCursoCruces();
                    that.popup_generado(0, "¡Horario Generado!");
                });
        },
        initControlHorarioGen: function (ini) {
            if (ini === 0) {//Al generar
                for (let i = 0; i < this.listaHorarios.length; i++)
                    this.horarioGenControl.push(0);
            } else { //Luego de evaluar cruces
                for (let i = 0; i < this.listaHorarios.length; i++)
                    this.horarioGenControl[i] = 0;
            }
            this.showHorario();
        },
        showHorario: function () {
            let cursoSecSel = [];
            this.resetTabla();
            for (let i = 0; i < this.listaHorarios.length; i++) { //para cada curso en el horario
                cursoSecSel.push(this.listaHorarios[i].sec[this.horarioGenControl[i]]);
                let seccionAct = cursoSecSel[i].horario;
                for (let j = 0; j < seccionAct.length; j++) { //para cada fila de horario segun la lista obtenida
                    let dia = seccionAct[j].dia;
                    let horaIni = parseInt(seccionAct[j].horaInicio);
                    let horaFin = parseInt(seccionAct[j].horaFin);
                    while (horaIni < horaFin) {
                        let diaInd;
                        switch (dia) {
                            case 'LU':
                                diaInd = 0;
                                break;
                            case 'MA':
                                diaInd = 1;
                                break;
                            case 'MI':
                                diaInd = 2;
                                break;
                            case 'JU':
                                diaInd = 3;
                                break;
                            case 'VI':
                                diaInd = 4;
                                break;
                            case 'SA':
                                diaInd = 5;
                                break;
                            default:
                        }
                        let valueHora = this.horarioGen[horaIni.toString()][diaInd];
                        let codigo = this.listaHorarios[i].codigo;
                        let seccion = cursoSecSel[i].seccion;
                        let tipoH = seccionAct[j].isTeoriaPractica.substr(0, 1);
                        let aula = seccionAct[j].aula;
                        aula = ""; //Solo en ciclos virtuales
                        let res;
                        if (valueHora == null) {//no hay curso en esa hora
                            res = codigo.concat("-", seccion, " \n (", tipoH, ") ", aula);
                        } else { //cruce
                            let recup = valueHora.split(" ");
                            if (recup[0] === "CRUCE:") {  //Si ya existe cruce con 2 cursos
                                res = recup[0].concat(" ", recup[1], "/(", tipoH, ") \n ", recup[3], " ", recup[4], " ", recup[5].substr(0, 7), " / ", codigo, "-", seccion, "]");
                                this.toCursoCruces(codigo);
                            } else {
                                if (recup[0] !== codigo.concat("-", seccion)) { //Si ya existe un curso
                                    res = "CRUCE: ".concat(recup[2], "/(", tipoH, ") \n [", recup[0], " / ", codigo, "-", seccion, "]");
                                    this.toCursoCruces(recup[0].substr(0, 5));
                                    this.toCursoCruces(codigo);
                                } else //Falso cruce: mismo curso. misma hora, diferente T-P
                                if (recup[2] !== "(".concat(tipoH, ")")) {
                                    res = recup[0].concat(" ", recup[2], " / (", tipoH, ") ", recup[3]);
                                }
                            }
                        }
                        this.horarioGen[horaIni.toString()][diaInd] = res;
                        horaIni++;
                    }
                }
            }
        },
        showHorarioInd: function (idx) { //Seleccionar un horario sin cruces usando posiciones
            this.horarioGenControl = this.horarioCrucesControls[idx];
            if (screen.width < 769) {
                let ln = document.createElement("a");
                ln.href = "#show";
                ln.click();
            }
            this.showHorario();
        },
        resetTabla: function () {
            for (let i = 8; i < 22; i++) {
                this.horarioGen[i.toString()] = [null, null, null, null, null, null];
            }
            this.cursoCruces = [];
        },
        generarCursoCruces: function () {
            let d = [];
            for (let i = 0; i < this.listaHorarios.length; i++) {
                d.push(this.listaHorarios[i].sec.length);
            }
            this.inspCombinaciones(0, d);
            this.initControlHorarioGen(1);
        },
        totalCombinaciones: function () {
            let t;
            if (this.listaHorarios.length > 0) {
                t = 1;
                for (let i = 0; i < this.listaHorarios.length; i++) {
                    t *= this.listaHorarios[i].sec.length;
                }
            } else {
                t = 0;
            }
            return t;
        },
        inspCombinaciones: function (idc, d) {
            if (idc < d.length) {
                for (let i = 0; i < d[idc]; i++) {
                    this.horarioGenControl[idc] = i;
                    if (!this.inspCombinaciones(idc + 1, d)) {
                        this.showHorario();
                        if (this.cursoCruces.length === 0 && idc + 1 === d.length) {
                            this.reportarCruces();
                        }
                    }
                }
            }
            return (idc < d.length)
        },
        reportarCruces: function () {
            let text = "";
            let tem = [];
            for (let i = 0; i < this.horarioGenControl.length; i++) {
                if (i !== 0)
                    text = text.concat(" | ");
                text = text.concat(this.listaHorarios[i].codigo, ":", this.listaHorarios[i].sec[this.horarioGenControl[i]].seccion);
                tem.push(this.horarioGenControl[i]);
            }
            this.horarioCrucesInfo.push(text);
            this.horarioCrucesControls.push(tem);
        },
        toCursoCruces: function (cruce) { //Almacenar cursos con cruces
            let existe = false;
            for (let i = 0; i < this.cursoCruces.length; i++) {
                if (cruce === this.cursoCruces[i]) {
                    existe = true;
                    break;
                }
            }
            if (!existe)
                this.cursoCruces.push(cruce);
        },
        intoCursoCruces: function (cruce) {
            for (let i = 0; i < this.cursoCruces.length; i++) {
                if (cruce === this.cursoCruces[i]) {
                    return true;
                }
            }
            return false;
        },
        color: function (curso) {
            let ind;
            //Deteccion de cruce
            if (this.intoCursoCruces(curso))
                ind = 10;
            else
                for (let i = 0; i < this.listaHorarios.length; i++) {
                    if (curso === this.listaHorarios[i].codigo) {
                        ind = i;
                        break;
                    } else if (curso === "CRUCE") {
                        ind = 10;
                        break;
                    }
                }
            return this.colorCursos[ind];
        },
        changeHorario: function (indc, op) {
            let j = this.horarioGenControl[indc];
            if (op === 1)
                if (j + 1 < this.listaHorarios[indc].sec.length) {
                    this.horarioGenControl[indc] = j + 1;
                } else {
                    this.horarioGenControl[indc] = 0;
                }
            else if (j === 0) {
                this.horarioGenControl[indc] = this.listaHorarios[indc].sec.length - 1;
            } else {
                this.horarioGenControl[indc] = j - 1;
            }
            this.showHorario();
        },
        isHorarioSel: function (ic, isc) {
            return this.horarioGenControl[ic] === isc;
        },
        downloadHorario: function () {
            let that = this;
            this.popup_generado(1, "Preparando descarga...");
            domtoimage.toPng(document.getElementById('horario-table'), {})
                .then(function (img) {
                    const date = new Date();

                    function pad(d) {
                        return (d < 10) ? '0' + d.toString() : d.toString();
                    }

                    let add = date.getFullYear().toString().concat(pad(date.getMonth() + 1), pad(date.getDate()), "-", pad(date.getHours()), pad(date.getMinutes()), pad(date.getSeconds()));
                    let link = document.createElement('a');
                    link.download = 'Horario_'.concat(add);
                    link.href = img;
                    link.click();
                    that.popup_generado(0, "¡Listo!");
                });
        },
        downToXlsx: function () {
            TableToExcel.convert(document.getElementById("horario-table"), {
                name: "horario_matrigen.xlsx",
                sheet: {
                    name: "Horario"
                }
            });
            bulmaToast.toast({
                message: 'Listo para descargar XLSX',
                type: 'is-success',
                duration: 2500,
                position: 'bottom-right',
                dismissible: false,
                animate: {in: 'fadeIn', out: 'fadeOut'},
            })
        },
        setPagina: function (pagina) {
            this.pagina = pagina;
            const navbarburger = document.getElementsByClassName("navbar-burger")[0];
            navbarburger.classList.remove('is-active');
            document.getElementById(navbarburger.dataset.target).classList.remove('is-active');
        },
        isPagina: function (pagina) {
            return (this.pagina === pagina);
        },
        setSubPag: function (subpagina) {
            this.subpagina = subpagina;
            const tab = document.getElementsByClassName("ficha");
            tab[subpagina % 2].classList.remove("is-active");
            tab[(subpagina + 1) % 2].classList.add("is-active");
        },
        isSubPag: function (subpagina) {
            return (this.subpagina === subpagina);
        },
        modalAddCurso: function (v) {
            const mod = document.getElementById("addCurso");
            if (v === 1) {
                mod.classList.add("is-active");
            } else {
                mod.classList.remove("is-active");
            }
        },
        popup_generado: function (v, text = "") {
            const mod = document.getElementById("loading2");
            const m1 = document.getElementById("m1");
            const m2 = document.getElementById("m2");
            if (text !== "") this.popupText = text;
            if (v === 1) {
                mod.classList.add("is-active");
                m2.style.display = "none";
            } else {
                m1.style.display = "none";
                m2.style.display = "";
                window.setTimeout(function () {
                    mod.classList.remove("is-active");
                    m2.style.display = "none";
                    m1.style.display = "";
                }, 1200);
            }
        },
        addCombinacion: function () {
            this.convertirCursoSec();
            this.listadoComb.push(JSON.parse(JSON.stringify(this.cursoSec)));
            this.$cookies.set("Comb", JSON.stringify(this.listadoComb));
            bulmaToast.toast({
                message: 'Guardado en Mis horarios',
                type: 'is-success',
                duration: 2000,
                position: 'bottom-right',
                dismissible: false,
                animate: {in: 'fadeIn', out: 'fadeOut'},
            });
        },
        removeCombinacion: function (i) {
            this.listadoComb.splice(i,1);
            this.$cookies.set("Comb", JSON.stringify(this.listadoComb));
            bulmaToast.toast({
                message: 'Combinación #'+(i+1)+' eliminada',
                type: 'is-danger',
                duration: 2000,
                position: 'bottom-right',
                dismissible: false,
                animate: {in: 'fadeIn', out: 'fadeOut'},
            });
        }
    }
})

document.addEventListener('DOMContentLoaded', () => {
    // Get all "navbar-burger" elements
    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);
    // Check if there are any navbar burgers
    if ($navbarBurgers.length > 0) {
        // Add a click event on each of them
        $navbarBurgers.forEach(el => {
            el.addEventListener('click', () => {
                // Get the target from the "data-target" attribute
                const target = el.dataset.target;
                const $target = document.getElementById(target);
                // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                el.classList.toggle('is-active');
                $target.classList.toggle('is-active');
            });
        });
    }
});