package pe.edu.uni.fiis.fcp.matrigen.model;

import java.util.List;

public class Seccion {
    private String seccion;
    private List<Horario> horario;

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    public List<Horario> getHorario() {
        return horario;
    }

    public void setHorario(List<Horario> horario) {
        this.horario = horario;
    }
}
