import Vue from 'vue'
import App from './App.vue'
import wb from "./registerServiceWorker";
import router from './router'
import vuetify from './plugins/vuetify'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueCookies from 'vue-cookies'

import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";

const options = {
    timeout: 2000,
    position: "bottom-right",
};

Vue.use(Toast, options);
Vue.use(VueCookies)
Vue.use(VueAxios, axios)
Vue.config.productionTip = false
Vue.prototype.$workbox = wb;

new Vue({
    router,
    vuetify,
    render: h => h(App)
}).$mount('#app')




