package pe.edu.uni.fiis.fcp.matrigen.dao.update;

import pe.edu.uni.fiis.fcp.matrigen.model.UpdNote;

import java.sql.*;
import java.util.Calendar;
import java.util.TimeZone;

public class UpdateNoteDaoImpl implements UpdateNoteDao {

    public UpdNote obtenerLastUpdate(Connection c) {
        UpdNote lastUpdNote = new UpdNote();
        try {
            StringBuffer sql = new StringBuffer();
            sql.append(" select u.id_update, u.fecha, u.hora, u.descripcion " +
                    " from updates u " +
                    " order by u.fecha desc, u.hora desc " +
                    " limit 1");
            Statement sentencia = c.createStatement();
            ResultSet rs = sentencia.executeQuery(sql.toString());
            Calendar tzCal = Calendar.getInstance(TimeZone.getTimeZone("America/Lima"));
            rs.next();
            lastUpdNote.setId(rs.getString("id_update"));
            lastUpdNote.setFecha(rs.getDate("fecha",tzCal));
            lastUpdNote.setHora(rs.getTime("hora"));
            lastUpdNote.setDescripcion(rs.getString("descripcion"));

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lastUpdNote;
    }

}