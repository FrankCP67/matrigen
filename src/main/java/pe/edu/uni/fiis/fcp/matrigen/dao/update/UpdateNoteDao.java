package pe.edu.uni.fiis.fcp.matrigen.dao.update;

import pe.edu.uni.fiis.fcp.matrigen.model.UpdNote;

import java.sql.Connection;

public interface UpdateNoteDao {
    public UpdNote obtenerLastUpdate(Connection c);
}
