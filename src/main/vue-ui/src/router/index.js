import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: () => import(/* webpackChunkName: "matrigen" */ '../views/Home'),
    },
    {
        path: '/showHorario',
        name: 'Mejores',
        props: true,
        meta: {
            requiresProps: true,
        },
        component: () => import(/* webpackChunkName: "matrigen" */ '../views/ShowHorario'),
    },
    {
        path: '/v2*',
        name: 'Matrigen v2',
        beforeEnter() {
            window.location.href = "/v2/index.html";
            self.location = "/v2";
        }
    },

    {
        path: '*',
        name: 'Not Found',
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
    scrollBehavior() {
        return {
            y: 0,
            behavior: 'smooth',
        }
    },
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresProps)) { //Si navegas a ShowHorario
        if (to.params.cursoSec === undefined)
            next({path: '/'})
        else
            next();
    }
    next();
});

export default router
