package pe.edu.uni.fiis.fcp.matrigen.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public abstract class Conexion {
    public static Connection getConnection() {
        Connection conex = null;
        try {
            Class.forName("org.postgresql.Driver");
            String jdbcDatabaseUrl = System.getenv("JDBC_DATABASE_URL"); //Heroku database
            conex = DriverManager.getConnection(jdbcDatabaseUrl);
            conex.setAutoCommit(false);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conex;
    }
}