package pe.edu.uni.fiis.fcp.matrigen.model;

import java.util.ArrayList;
import java.util.List;

public class CursoSec {
    private List<String> curso;
    private List<String> sec;

    public List<String> getCurso() {
        return curso;
    }

    public void setCurso(List<String> curso) {
        this.curso = curso;
    }

    public List<String> getSec() {
        return sec;
    }

    public void setSec(List<String> sec) {
        this.sec = sec;
    }

    public List<String> generarCursoSeccion(){
        List<String> list= new ArrayList<String>();
        for (int i=0; i<curso.size(); i++) {
            StringBuffer conc= new StringBuffer();
            conc.append(curso.get(i));
            conc.append(sec.get(i));
            list.add(conc.toString());
        }
        return list;
    }
}
