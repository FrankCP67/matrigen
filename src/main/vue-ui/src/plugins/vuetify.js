import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import colors from 'vuetify/lib/util/colors'

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        themes: {
            light: {
                background: colors.blue.accent4,
                dTeal: colors.teal.lighten2,
                dRed: colors.red.accent2,
                dOrange: colors.orange.accent2,
                dGreen: colors.green.accent2,
                iLime: colors.lime.darken2,
                dYellow: colors.yellow.lighten1,
                dIndigo: colors.indigo.accent3,
                dDeepOrange: colors.deepOrange.accent2,
                iBlue: colors.blue.darken4,
            },
            dark: {
                background: colors.blue.darken4,
                dTeal: colors.teal.darken4,
                dRed: colors.red.darken1,
                dOrange: colors.orange.darken3,
                dGreen: colors.green.darken3,
                iLime: colors.lime.accent2,
                dYellow: colors.yellow.darken4,
                dIndigo: colors.indigo.darken4,
                dDeepOrange: colors.deepOrange.accent3,
                iBlue: colors.blue.accent3,
            },
        },
    },
    breakpoint: {
        mobileBreakpoint: 'sm' // This is equivalent to a value of 960
    },
});
