package pe.edu.uni.fiis.fcp.matrigen.controller;

import pe.edu.uni.fiis.fcp.matrigen.model.Curso;
import pe.edu.uni.fiis.fcp.matrigen.service.SingletonService;
import pe.edu.uni.fiis.fcp.matrigen.util.Json;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name= "CursoObtenerController", urlPatterns = "/api/obtener-cur")
public class CursoObtenerController extends HttpServlet {

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = Json.getJson(req);
        Curso curso = Json.getInstance().readValue(data,Curso.class);
        List<Curso> cursos = SingletonService.getHorarioService().obtenerCursos();
        Json.envioJson(cursos,resp);
    }
}