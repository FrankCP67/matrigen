// vue.config.js
module.exports = {
    // proxy all webpack dev-server requests starting with /api
    // to our Spring Boot backend (localhost:8098) using http-proxy-middleware
    // see https://cli.vuejs.org/config/#devserver-proxy
    devServer: {
        port: 8082,
        proxy: {
            '/api': {
                target: 'http://localhost:8080',
                ws: true,
                changeOrigin: true,
            }
        }
    },

    chainWebpack: config => {
        config
            .plugin('html')
            .tap(args => {
                args[0].title = 'MatriGEN - Generador de horarios';
                return args;
            })
    },

    // Change build paths to make them Maven compatible
    // see https://cli.vuejs.org/config/
    outputDir: 'target/dist',

    assetsDir: 'static',

    transpileDependencies: [
      'vuetify'
    ],
    configureWebpack: {
        module: {
            exprContextCritical: false
        }
    },

    //PWA
    pwa: {
        name: 'MatriGEN',
        themeColor: '#2962FF',
        msTileColor: '#f8e592',
        appleMobileWebAppCache: 'yes',
        manifestOptions: {
            short_name: 'MatriGEN',
            background_color: '#2962FF',
            description: 'MatriGEN es un Generador de Horarios de Matricula que permite agilizar la creación de horarios según los cursos seleccionados.',
            lang: 'es',
            display: 'standalone',
            orientation: 'natural',
            icons: [
                {
                    "src": "./img/icons/android-chrome-192x192.png",
                    "sizes": "192x192",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/android-chrome-512x512.png",
                    "sizes": "512x512",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/android-chrome-maskable-192x192.png",
                    "sizes": "192x192",
                    "type": "image/png",
                    "purpose": "maskable"
                },
                {
                    "src": "./img/icons/android-chrome-maskable-512x512.png",
                    "sizes": "512x512",
                    "type": "image/png",
                    "purpose": "maskable"
                },
                {
                    "src": "./img/icons/apple-touch-icon-60x60.png",
                    "sizes": "60x60",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/apple-touch-icon-76x76.png",
                    "sizes": "76x76",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/apple-touch-icon-120x120.png",
                    "sizes": "120x120",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/apple-touch-icon-152x152.png",
                    "sizes": "152x152",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/apple-touch-icon-180x180.png",
                    "sizes": "180x180",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/apple-touch-icon.png",
                    "sizes": "180x180",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/favicon-16x16.png",
                    "sizes": "16x16",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/favicon-32x32.png",
                    "sizes": "32x32",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/msapplication-icon-144x144.png",
                    "sizes": "144x144",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/mstile-150x150.png",
                    "sizes": "150x150",
                    "type": "image/png"
                }
            ],
            shortcuts: [
                {
                    "name": "Iniciar Matrigen v3",
                    "url": "/",
                    "icons": [
                        {
                            "src": "./img/icons/icon-v3.png",
                            "type": "image/png",
                            "sizes": "192x192"
                        }
                    ]
                },
                {
                    "name": "Versión Anterior (v2)",
                    "url": "/v2",
                    "icons": [
                        {
                            "src": "./img/icons/icon-v2.png",
                            "type": "image/png",
                            "sizes": "192x192"
                        }
                    ]
                },
            ],
            scope:  '/'
        },
        workboxOptions: {
            navigateFallback: 'index.html',
            navigateFallbackBlacklist: [new RegExp('(?=\\/v2).*$')],
        },
    }
}
