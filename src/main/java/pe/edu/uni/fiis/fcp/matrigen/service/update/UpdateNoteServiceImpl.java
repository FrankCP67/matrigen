package pe.edu.uni.fiis.fcp.matrigen.service.update;

import pe.edu.uni.fiis.fcp.matrigen.dao.SingletonDao;
import pe.edu.uni.fiis.fcp.matrigen.model.UpdNote;
import pe.edu.uni.fiis.fcp.matrigen.service.Conexion;

import java.sql.Connection;
import java.sql.SQLException;

public class UpdateNoteServiceImpl implements UpdateNoteService {

    public UpdNote obtenerLastUpdate() {
        Connection connection= Conexion.getConnection();
        UpdNote lastUpdNote = SingletonDao.getUpdateNoteDao().obtenerLastUpdate(connection);
        try {
            connection.commit();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lastUpdNote;
    }

}
