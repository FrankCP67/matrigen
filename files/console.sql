create database relacion19_2;

create table cursos(
    codigo varchar(5) primary key,
    nombre varchar(40),
    creditos numeric(1,0),
    especialidad varchar(2),
    tipo varchar(15),
    sist_eval varchar(1),
    ciclo numeric(2,0)
);

create table curso_seccion(
    seccion varchar(1),
    codigo_curso varchar(5) references cursos(codigo),
    curso_seccion varchar(6) primary key
);

create table horarios(
    curso_seccion varchar(6) references curso_seccion(curso_seccion),
    dia varchar(9),
    hora_ini varchar(4),
    hora_fin varchar(4),
    t_p varchar(6),
    aula varchar(15),
    docente varchar(40)
);

select * from cursos;
select * from curso_seccion;
select * from horarios;

select curso_seccion, dia, hora_ini, hora_fin, t_p, aula, docente  from horarios where false or curso_seccion='BFI01W';
select curso_seccion, dia, hora_ini, hora_fin, t_p, aula, docente  from horarios where false or curso_seccion='BFI01V' or curso_seccion='BFI01W' order by curso_seccion;
select h.curso_seccion, h.dia, h.hora_ini, h.hora_fin, h.t_p, h.aula, h.docente, c.nombre, c.creditos, c.especialidad,c.tipo, c.sist_eval,c.ciclo from horarios h inner join curso_seccion cs inner join cursos c on cs.codigo_curso = c.codigo on h.curso_seccion = cs.curso_seccion where false or h.curso_seccion='BFI01V' or h.curso_seccion='BFI01W' or h.curso_seccion='FB301U' order by h.curso_seccion;

select seccion, codigo_curso, curso_seccion from curso_seccion where codigo_curso='BFI01';

select codigo, nombre, creditos, especialidad, tipo, sist_eval, ciclo from cursos where curso=;

insert into cursos (codigo, nombre, creditos, especialidad, tipo, sist_eval, ciclo)
values ('BFI01','Fisica 1',5,'I2','Ob','F',3);

insert into cursos (codigo, nombre, creditos, especialidad, tipo, sist_eval, ciclo)
values ('FB301','Matemática Discreta',3,'I2','Ob','G',3);

insert into curso_seccion (seccion, codigo_curso, curso_seccion) VALUES ('W','BFI01','BFI01W');
insert into curso_seccion (seccion, codigo_curso, curso_seccion) VALUES ('V','FB301','FB301V');

insert into horarios (curso_seccion, dia, hora_ini, hora_fin, t_p, aula, docente)
VALUES ('FB301U','VI','15','17','T','S4-202','prof04');

select c.codigo,c.nombre,c.creditos,cs.seccion from cursos c inner join curso_seccion cs on c.codigo = cs.codigo_curso order by codigo_curso,seccion;

select codigo_curso, seccion from curso_seccion order by codigo_curso,seccion;
