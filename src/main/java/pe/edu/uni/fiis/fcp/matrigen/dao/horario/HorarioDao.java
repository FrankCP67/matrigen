package pe.edu.uni.fiis.fcp.matrigen.dao.horario;

import pe.edu.uni.fiis.fcp.matrigen.model.Curso;

import java.sql.Connection;
import java.util.List;

public interface HorarioDao {
    public List<Curso> obtenerCursos(Connection c);
    public List<Curso> obtenerHorarios(List<String> cursoSec,Connection c);
}
