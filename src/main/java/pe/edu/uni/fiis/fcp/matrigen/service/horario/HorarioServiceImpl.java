package pe.edu.uni.fiis.fcp.matrigen.service.horario;

import pe.edu.uni.fiis.fcp.matrigen.dao.SingletonDao;
import pe.edu.uni.fiis.fcp.matrigen.model.Curso;
import pe.edu.uni.fiis.fcp.matrigen.service.Conexion;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class HorarioServiceImpl implements HorarioService {

    public List<Curso> obtenerCursos() {
        Connection connection= Conexion.getConnection();
        List<Curso> lista = SingletonDao.getHorarioDao().obtenerCursos(connection);
        try {
            connection.commit();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Curso> obtenerHorarios(List<String> cursoSec) {
        Connection connection= Conexion.getConnection();
        List<Curso> lista = SingletonDao.getHorarioDao().obtenerHorarios(cursoSec,connection);
        try {
            connection.commit();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lista;
    }
}
