package pe.edu.uni.fiis.fcp.matrigen.model;

import java.util.List;

public class Curso {
    private String codigo;
    private String nombre;
    private Integer creditos;
    private List<String> secciones;
    private List<Seccion> sec;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getCreditos() {
        return creditos;
    }

    public void setCreditos(Integer creditos) {
        this.creditos = creditos;
    }

    public List<String> getSecciones() {
        return secciones;
    }

    public void setSecciones(List<String> secciones) {
        this.secciones = secciones;
    }

    public List<Seccion> getSec() {
        return sec;
    }

    public void setSec(List<Seccion> sec) {
        this.sec = sec;
    }
}
