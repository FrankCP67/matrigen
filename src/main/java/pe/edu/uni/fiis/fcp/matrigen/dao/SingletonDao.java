package pe.edu.uni.fiis.fcp.matrigen.dao;

import pe.edu.uni.fiis.fcp.matrigen.dao.horario.HorarioDao;
import pe.edu.uni.fiis.fcp.matrigen.dao.horario.HorarioDaoImpl;
import pe.edu.uni.fiis.fcp.matrigen.dao.update.UpdateNoteDao;
import pe.edu.uni.fiis.fcp.matrigen.dao.update.UpdateNoteDaoImpl;

public abstract class SingletonDao {
    private static HorarioDao horarioDao=null;
    public static HorarioDao getHorarioDao(){
        if(horarioDao==null){
            horarioDao=new HorarioDaoImpl();
        }
        return horarioDao;
    }
    private static UpdateNoteDao updateNoteDao=null;
    public static UpdateNoteDao getUpdateNoteDao(){
        if(updateNoteDao==null){
            updateNoteDao=new UpdateNoteDaoImpl();
        }
        return updateNoteDao;
    }
}