package pe.edu.uni.fiis.fcp.matrigen.model;

public class Horario {
    private String dia;
    private String horaInicio;
    private String horaFin;
    private String isTeoriaPractica;
    private String aula;
    private String docente;

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    public String getIsTeoriaPractica() {
        return isTeoriaPractica;
    }

    public void setIsTeoriaPractica(String isTeoriaPractica) {
        this.isTeoriaPractica = isTeoriaPractica;
    }

    public String getAula() {
        return aula;
    }

    public void setAula(String aula) {
        this.aula = aula;
    }

    public String getDocente() {
        return docente;
    }

    public void setDocente(String docente) {
        this.docente = docente;
    }
}
