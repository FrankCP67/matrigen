package pe.edu.uni.fiis.fcp.matrigen.service.horario;

import pe.edu.uni.fiis.fcp.matrigen.model.Curso;

import java.util.List;

public interface HorarioService {
    public List<Curso> obtenerCursos();
    public List<Curso> obtenerHorarios(List<String> cursoSec);
}
