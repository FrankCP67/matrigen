package pe.edu.uni.fiis.fcp.matrigen.dao.horario;

import pe.edu.uni.fiis.fcp.matrigen.model.Curso;
import pe.edu.uni.fiis.fcp.matrigen.model.Horario;
import pe.edu.uni.fiis.fcp.matrigen.model.Seccion;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class HorarioDaoImpl implements HorarioDao {

    public List<Curso> obtenerCursos(Connection c) {
        List<Curso> lista = new ArrayList<Curso>();
        try {
            StringBuffer sql = new StringBuffer();
            sql.append(" select c.codigo,c.nombre,c.creditos,cs.seccion " +
                    " from cursos c inner join curso_seccion cs on c.codigo = cs.codigo_curso " +
                    " order by codigo_curso,seccion ");
            Statement sentencia = c.createStatement();
            ResultSet rs = sentencia.executeQuery(sql.toString());
            String codigoAnt = "";
            int cont = 0;
            List<String> listaSec = new ArrayList<String>();
            Curso curso = new Curso();
            while (rs.next()) {
                if (!codigoAnt.equals(rs.getString("codigo"))) { //Accion si es un nuevo curso
                    if (cont != 0) {
                        //Almacenar el curso anterior
                        curso.setSecciones(listaSec);
                        lista.add(curso);
                        curso = new Curso();
                        listaSec = new ArrayList<String>();
                    }
                    //Nuevo curso
                    curso.setCodigo(rs.getString("codigo"));
                    curso.setNombre(rs.getString("nombre"));
                    curso.setCreditos(rs.getInt("creditos"));
                    String stSec = rs.getString("seccion");
                    listaSec.add(stSec); //Guarda la primera seccion de un curso
                    //Preparando para el siguiente paso
                    codigoAnt = curso.getCodigo();
                } else { //Accion si es el mismo curso
                    String stSec = rs.getString("seccion");
                    listaSec.add(stSec);
                }
                cont++;
            }
            if (cont != 0) {
                //Almacenar el curso anterior ULTIMO
                curso.setSecciones(listaSec);
                lista.add(curso);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Curso> obtenerHorarios(List<String> cursoSec, Connection c) {
        List<Curso> lista = new ArrayList<Curso>();
        try {
            StringBuffer sql = new StringBuffer();
            sql.append(" select h.curso_seccion, h.dia, h.hora_ini, h.hora_fin, " +
                    " h.t_p, h.aula, h.docente, c.nombre, c.creditos " +
                    " from horarios h inner join " +
                    " curso_seccion cs inner join cursos c on cs.codigo_curso = c.codigo " +
                    " on h.curso_seccion = cs.curso_seccion where false ");
            for (int i = 0; i < cursoSec.size(); i++) {
                sql.append(" or h.curso_seccion=? ");
            }
            sql.append(" order by h.curso_seccion ");
            PreparedStatement sentencia = c.prepareStatement(sql.toString());
            for (int i = 0; i < cursoSec.size(); i++) {
                sentencia.setString(i+1, cursoSec.get(i));
            }
            ResultSet rs = sentencia.executeQuery();

            String codigoAntC = "";
            String codigoAntS = "";
            int cont = 0;
            Curso curso = new Curso();
            List<Seccion> secciones = new ArrayList<Seccion>();
            Seccion seccion = new Seccion();
            List<Horario> horarios = new ArrayList<Horario>();
            Horario horario = new Horario();
            while (rs.next()) {
                if (!codigoAntC.equals(rs.getString("curso_seccion").substring(0, 5))) { //Accion si es un nuevo curso
                    if (cont != 0) {
                        //Almacenar la seccion anterior
                        seccion.setHorario(horarios);
                        horarios = new ArrayList<Horario>(); //Generar
                        secciones.add(seccion);
                        seccion = new Seccion(); //Generar
                        //Almacenar el curso anterior
                        curso.setSec(secciones);
                        secciones = new ArrayList<Seccion>(); //Generar
                        lista.add(curso);
                        curso = new Curso(); //Generar
                    }
                    //Nuevo curso
                    curso.setCodigo(rs.getString("curso_seccion").substring(0, 5));
                    curso.setNombre(rs.getString("nombre"));
                    curso.setCreditos(rs.getInt("creditos"));
                    //Nueva seccion
                    seccion.setSeccion(rs.getString("curso_seccion").substring(5, 6));
                } else { //Accion si es el mismo curso
                    if (!codigoAntS.equals(rs.getString("curso_seccion").substring(5, 6))) { //Accion si es una nueva seccion
                        //Almacenar la seccion anterior
                        seccion.setHorario(horarios);
                        horarios = new ArrayList<Horario>(); //Generar
                        secciones.add(seccion);
                        seccion = new Seccion(); //Generar
                        //Nueva seccion
                        seccion.setSeccion(rs.getString("curso_seccion").substring(5, 6));
                    }
                }
                //Nuevo Horario
                horario.setDia(rs.getString("dia"));
                horario.setHoraInicio(rs.getString("hora_ini"));
                horario.setHoraFin(rs.getString("hora_fin"));
                horario.setIsTeoriaPractica(rs.getString("t_p"));
                horario.setAula(rs.getString("aula"));
                horario.setDocente(rs.getString("docente"));
                horarios.add(horario); //Guarda el nuevo horario de un curso
                horario = new Horario();
                codigoAntC = curso.getCodigo();
                codigoAntS = seccion.getSeccion();
                cont++;
            }
            if (cont != 0) {
                //Almacenar la ultima seccion del ultimo curso
                seccion.setHorario(horarios);
                secciones.add(seccion);
                //Almacenar el ultimo curso
                curso.setSec(secciones);
                lista.add(curso);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lista;
    }
}