package pe.edu.uni.fiis.fcp.matrigen.controller;

import pe.edu.uni.fiis.fcp.matrigen.model.UpdNote;
import pe.edu.uni.fiis.fcp.matrigen.service.SingletonService;
import pe.edu.uni.fiis.fcp.matrigen.util.Json;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name= "GetLastUpdateController", urlPatterns = "/api/gluc")
public class GetLastUpdateController extends HttpServlet {

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = Json.getJson(req);
        //String savedLocalUpd = Json.getInstance().readValue(data,String.class);
        UpdNote updNote = SingletonService.getUpdateNoteService().obtenerLastUpdate();
        Json.envioJson(updNote,resp);
    }
}