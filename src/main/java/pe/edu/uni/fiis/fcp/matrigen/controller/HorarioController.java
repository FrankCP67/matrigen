package pe.edu.uni.fiis.fcp.matrigen.controller;

import pe.edu.uni.fiis.fcp.matrigen.model.Curso;
import pe.edu.uni.fiis.fcp.matrigen.model.CursoSec;
import pe.edu.uni.fiis.fcp.matrigen.service.SingletonService;
import pe.edu.uni.fiis.fcp.matrigen.util.Json;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name= "HorarioController", urlPatterns = "/api/obtener-hora")
public class HorarioController extends HttpServlet {

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String data = Json.getJson(req);
        CursoSec cursoSec = Json.getInstance().readValue(data, CursoSec.class);

        List<Curso> cursos = SingletonService.getHorarioService().obtenerHorarios(cursoSec.generarCursoSeccion());
        Json.envioJson(cursos,resp);
    }
}
